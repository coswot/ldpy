## Release Notes

All notable changes to the "linked-data-python" are documented below.

## [0.0.3] - 2023-02-24

- use `ideas` import hooks to enable using `.ldpy` files from import statements
- added interactive ldpy console
- added line mapping to point to the right line in the `.ldpy` file on error
- corrected small bugs in the parser

## [0.0.2] - 2022-04-30

- corrected many small bugs
- added many examples
- add `-d` functionality

## [0.0.1] - 2022-04-29

- Initial release
